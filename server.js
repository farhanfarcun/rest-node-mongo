const express = require('express');
const bodyParser = require('body-parser');
const app = express();
const mongoose = require('mongoose');

app.use(bodyParser.urlencoded({extended: false}));

mongoose.connect('mongodb://127.0.0.1:27017/books', {useNewUrlParser: true});
const db = mongoose.connection;
db.on('error', (error) => console.error(error));
db.once('open', () => console.log('Connected to database'));

app.use(express.json());

const booksRouter = require('./routes/books');
app.use('/books', booksRouter);

app.listen(process.env.PORT || 3000, () => {
  console.log('Server Running at Port 3000');
});

module.exports = app;
