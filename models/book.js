const mongoose = require('mongoose');

const bookSchema = new mongoose.Schema({
  'id': {
    type: Number,
  },
  'title': {
    type: String,
  },
  'author': {
    type: String,
  },
  'isbn': {
    type: String,
  },
  'publishedOn': {
    type: Number,
  },
  'numberOfPages': {
    type: Number,
  },
});

module.exports = mongoose.model('Book', bookSchema);
