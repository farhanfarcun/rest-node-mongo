const app = require('../server');
const chai = require('chai');
const chaiHttp = require('chai-http');

const {expect} = chai;
chai.use(chaiHttp);
describe('Server!', () => {
  it('GET /books', (done) => {
    chai
        .request(app)
        .get('/books')
        .end((err, res) => {
          expect(res).to.have.status(200);
          done();
        });
  });

  it('GET /books/:id', (done) => {
    chai
        .request(app)
        .get('/books/' + 664768879)
        .end((err, res) => {
          expect(res).to.have.status(200);
          expect(res).not.null;
          done();
        });
  });

  it('POST /books', (done) => {
    chai
        .request(app)
        .post('/books')
        .send({
          title: 'test-title',
          author: 'test-author',
          isbn: 'test-isbn',
          publishedOn: 1999,
          numberOfPages: 1111,
        })
        .end((err, res) => {
          expect(res).to.have.status(201);
          expect(res.body.id).not.null;
          done();
        });
  });

  it('PUT /books/:id', (done) => {
    chai
        .request(app)
        .put('/books/' + 664768879)
        .send({
          title: 'test-title',
          author: 'test-author',
          isbn: 'test-isbn',
          publishedOn: 1999,
          numberOfPages: 1111,
        })
        .end((err, res) => {
          expect(res).to.have.status(200);
          done();
        });
  });

  it('DELETE /books/:id', (done) => {
    chai
        .request(app)
        .delete('/books/' + 664768879)
        .end((err, res) => {
          expect(res).to.have.status(200);
          done();
        });
  });
});
